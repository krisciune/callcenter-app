package itakademija.java2015.myapp.ui.models;

import itakademija.java2015.myapp.entities.Survey;

import java.io.Serializable;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SurveyModel implements Serializable {

    private static final long serialVersionUID = -8131673305859876205L;

    static final Logger log = LoggerFactory.getLogger(ClientModel.class);

    public void destroy() {
        log.warn("Bean destroyed");
    }

    @Valid
    private Survey currentSurvey;

    public void init() {
        log.warn("Bean created");
        currentSurvey = new Survey();
    }

	public Survey getCurrentSurvey() {
		return currentSurvey;
	}

	public void setCurrentSurvey(Survey currentSurvey) {
		this.currentSurvey = currentSurvey;
	}

}

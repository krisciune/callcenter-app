package itakademija.java2015.myapp.ui.controllers;

import itakademija.java2015.myapp.entities.Survey;
import itakademija.java2015.myapp.services.SurveyService;
import itakademija.java2015.myapp.ui.models.ClientModel;
import itakademija.java2015.myapp.ui.models.SurveyModel;

import java.io.Serializable;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SurveyController implements Serializable {

    private static final long serialVersionUID = -1389684115871062390L;

    static final Logger log = LoggerFactory.getLogger(SurveyController.class);

    public void init() {
        log.warn("Bean created");
    }

    public void destroy() {
        log.warn("Bean destroyed");
    }

    private ClientModel clientModel;
    private SurveyModel surveyModel;
    private SurveyService surveyService;

    public void create() {
        surveyModel.setCurrentSurvey(new Survey());
    }

    public void update(Survey survey) {
        surveyModel.setCurrentSurvey(survey);
    }

    public String save() {
        surveyModel.getCurrentSurvey().setClient(clientModel.getCurrentClient());
        surveyModel.getCurrentSurvey().setSurveyDate(new Date());
        System.out.println("saving date: " + surveyModel.getCurrentSurvey().getSurveyDate());
        surveyService.save(surveyModel.getCurrentSurvey());
        surveyModel.setCurrentSurvey(new Survey());
        return ClientController.NAV_LIST_SURVEYS;
    }

    public String delete(Survey survey) {
        surveyService.delete(survey);
        return ClientController.NAV_LIST_SURVEYS;
    }

    public String cancel() {
        surveyModel.setCurrentSurvey(new Survey());
        return ClientController.NAV_LIST_SURVEYS;
    }

    public SurveyModel getSurveyModel() {
        return surveyModel;
    }
    public void setSurveyModel(SurveyModel surveyModel) {
        this.surveyModel = surveyModel;
    }

    public SurveyService getSurveyService() {
        return surveyService;
    }

    public void setSurveyService(SurveyService surveyService) {
        this.surveyService = surveyService;
    }

	public ClientModel getClientModel() {
		return clientModel;
	}

	public void setClientModel(ClientModel clientModel) {
		this.clientModel = clientModel;
	}

}

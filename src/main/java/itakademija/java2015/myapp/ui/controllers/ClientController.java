package itakademija.java2015.myapp.ui.controllers;

import itakademija.java2015.myapp.entities.Client;
import itakademija.java2015.myapp.entities.Survey;
import itakademija.java2015.myapp.services.ClientService;
import itakademija.java2015.myapp.ui.models.ClientModel;

import java.io.Serializable;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ClientController implements Serializable {

    private static final long serialVersionUID = -8143077418240345336L;

    static final Logger log = LoggerFactory.getLogger(ClientController.class);

    public void init() {
        log.warn("Bean created");
    }

    public void destroy() {
        log.warn("Bean destroyed");
    }

    public static final String NAV_LIST_CLIENTS = "list-clients";
    public static final String NAV_LIST_SURVEYS = "list-surveys";

    private ClientModel clientModel;
    private ClientService clientService;

    public void create() {
        clientModel.setCurrentClient(new Client());
    }

    public String delete(Client client) {
        clientService.delete(client);
        return NAV_LIST_CLIENTS;
    }

    public List<Client> getClientList() {
        return clientService.findAll();
    }

    public List<Survey> findSurveysByClient() {
        return clientModel.getCurrentClient().getSurveys();
    }

    public List<Survey> getFreshSurveyList() {
        Long currentClientId = clientModel.getCurrentClient().getId();
        if (currentClientId == null) return null;
        return clientService.findById(currentClientId).getSurveys();
    }

    public String update(Client client) {
        clientModel.setCurrentClient(client);
        return NAV_LIST_SURVEYS;
    }

    public String save() {
        clientService.save(clientModel.getCurrentClient());
        return NAV_LIST_CLIENTS;
    }

    public String cancel() {
        clientModel.setCurrentClient(new Client());
        return NAV_LIST_CLIENTS;
    }

	public ClientModel getClientModel() {
		return clientModel;
	}

	public void setClientModel(ClientModel clientModel) {
		this.clientModel = clientModel;
	}

	public ClientService getClientService() {
		return clientService;
	}

	public void setClientService(ClientService clientService) {
		this.clientService = clientService;
	}

}

package itakademija.java2015.myapp.ui.converters;

import itakademija.java2015.myapp.entities.Client;
import itakademija.java2015.myapp.services.ClientService;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ClientConverter implements Converter {

    static final Logger log = LoggerFactory.getLogger(ClientConverter.class);

    public void init() {
        log.warn("Bean created");
    }

    public void destroy() {
        log.warn("Bean destroyed");
    }

    private ClientService clientService;

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        try {
            Object obj =  clientService.findById(Long.valueOf(value));
            return obj;
        } catch (Exception e) {
            throw new ConverterException(new FacesMessage(String.format("Cannot convert %s to Client", value)), e);
        }
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        if (!(value instanceof Client)) {
            return null;
        }
       String s = String.valueOf(((Client) value).getId());
       return s;
    }

    public ClientService getClientService() {
        return clientService;
    }

    public void setClientService(ClientService clientService) {
        this.clientService = clientService;
    }

}

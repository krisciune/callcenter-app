package itakademija.java2015.myapp.ui.validators;

import java.math.BigDecimal;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SatisfactionValidator implements Validator {

    private static final Logger log = LoggerFactory.getLogger(SatisfactionValidator.class);

    public void init() {
        log.warn("Bean created");
    }

    public void destroy() {
        log.warn("Bean destroyed");
    }

    @Override
    public void validate(FacesContext context, UIComponent component, Object value) throws ValidatorException {
        int amount = 0;
        if (value instanceof Integer) {
            amount = (Integer) value;
        } else {
            FacesMessage message = new FacesMessage("Not a number");
            message.setSeverity(FacesMessage.SEVERITY_ERROR);
            throw new ValidatorException(message);
        }

        if (amount < 1 || amount > 10) {
            FacesMessage message = new FacesMessage("Please write a number from 1 to 10");
            message.setSeverity(FacesMessage.SEVERITY_ERROR);
            throw new ValidatorException(message);
        }
    }
}

package itakademija.java2015.myapp.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import itakademija.java2015.myapp.entities.Survey;
import itakademija.java2015.myapp.entities.repositories.SurveyRepository;

public class SurveyService {

    static final Logger log = LoggerFactory.getLogger(SurveyService.class);

    public void init() {
        log.warn("Bean created");
    }

    public void destroy() {
        log.warn("Bean destroyed");
    }

    private SurveyRepository surveyRepo;

    public void save(Survey survey) {
        surveyRepo.save(survey);
    }

    public void delete(Survey survey) {
        surveyRepo.delete(survey);
    }

	public SurveyRepository getSurveyRepo() {
		return surveyRepo;
	}

	public void setSurveyRepo(SurveyRepository surveyRepo) {
		this.surveyRepo = surveyRepo;
	}

}

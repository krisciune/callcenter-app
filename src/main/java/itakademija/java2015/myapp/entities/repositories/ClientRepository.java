package itakademija.java2015.myapp.entities.repositories;

import itakademija.java2015.myapp.entities.Client;

import java.util.List;

public interface ClientRepository {

    void save(Client newClient);

    void delete(Client client);

    List<Client> findAll();

    Client findById(Long id);

    Long countClients();

}

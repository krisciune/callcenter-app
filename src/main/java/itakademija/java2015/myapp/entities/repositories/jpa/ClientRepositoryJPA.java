package itakademija.java2015.myapp.entities.repositories.jpa;

import itakademija.java2015.myapp.entities.Client;
import itakademija.java2015.myapp.entities.repositories.ClientRepository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ClientRepositoryJPA implements ClientRepository {

    static final Logger log = LoggerFactory.getLogger(ClientRepositoryJPA.class);

    public void init() {
        log.warn("Bean created");
    }

    public void destroy() {
        log.warn("Bean destroyed");
    }

    private EntityManagerFactory entityManagerFactory;

    public void setEntityManagerFactory(EntityManagerFactory emf) {
        this.entityManagerFactory = emf;
    }

    private EntityManager getEntityManager() {
        return entityManagerFactory.createEntityManager();
    }

    @Override
    public void save(Client client) {
        EntityManager entityManager = getEntityManager();
        try {
            entityManager.getTransaction().begin();
            if (!entityManager.contains(client))
                client = entityManager.merge(client);
            entityManager.persist(client);
            entityManager.getTransaction().commit();
        } finally {
            entityManager.close();
        }

    }

    @Override
    public void delete(Client client) {
        EntityManager entityManager = getEntityManager();
        try {
            entityManager.getTransaction().begin();
            client = entityManager.merge(client);
            entityManager.remove(client);
            entityManager.getTransaction().commit();
        } finally {
            entityManager.close();
        }

    }

    @Override
    public List<Client> findAll() {
        EntityManager entityManager = getEntityManager();
        try {
            CriteriaBuilder cb = entityManager.getCriteriaBuilder();
            CriteriaQuery<Client> cq = cb.createQuery(Client.class);
            Root<Client> root = cq.from(Client.class);
            cq.select(root);
            TypedQuery<Client> q = entityManager.createQuery(cq);
            return q.getResultList();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public Long countClients() {
        EntityManager entityManager = getEntityManager();
        try {
            CriteriaBuilder cb = entityManager.getCriteriaBuilder();
            CriteriaQuery<Long> cq = cb.createQuery(Long.class);
            Root<Client> root = cq.from(Client.class);
            cq.select(cb.count(root));
            TypedQuery<Long> q = entityManager.createQuery(cq);
            return q.getSingleResult();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public Client findById(Long id) {
        EntityManager entityManager = getEntityManager();
        try {
            CriteriaBuilder cb = entityManager.getCriteriaBuilder();
            CriteriaQuery<Client> cq = cb.createQuery(Client.class);
            Root<Client> root = cq.from(Client.class);
            cq.where(cb.equal(root.get("id"), id));
            TypedQuery<Client> q = entityManager.createQuery(cq);
            return q.getSingleResult();
        } finally {
            entityManager.close();
        }
    }

}

package itakademija.java2015.myapp.entities.repositories;

import itakademija.java2015.myapp.entities.Client;
import itakademija.java2015.myapp.entities.Survey;

import java.util.List;

public interface SurveyRepository {

    public void save(Survey survey);

    public void delete(Survey survey);

    public List<Survey> findAll();

    public List<Survey> findAllByClient(Client client);

}
